from tmp_connection_psql import tmp_connection
from questions import (
    question_1_rel,
    question_2_rel,
    question_3_rel,
    question_4_rel,
    question_5_rel,
    question_6_rel,
    question_1_json,
    question_2_json,
    question_3_json,
    question_4_json,
    question_5_json,
    question_6_json,
    print_consigne,
)
from rich.console import Console

CONSOLE = Console()

def main():
    CONSOLE.print("Hello :waving_hand:")
    with tmp_connection("password", "questions/relational/rel.sql") as conn:
        print_consigne()
        CONSOLE.print(f"\n[bold]{'_'*30}RELATIONAL{'_'*30}[/bold]")
        CONSOLE.print("\n[bold]QUESTION 1[/bold]:question_mark:")
        question_1_rel(conn)
        CONSOLE.print("\n[bold]QUESTION 2[/bold]:question_mark:")
        question_2_rel(conn)
        CONSOLE.print("\n[bold]QUESTION 3[/bold]:question_mark:")
        question_3_rel(conn)
        CONSOLE.print("\n[bold]QUESTION 4[/bold]:question_mark:")
        question_4_rel(conn)
        CONSOLE.print("\n[bold]QUESTION 5[/bold]:question_mark:")
        question_5_rel(conn)
        CONSOLE.print("\n[bold]QUESTION 6[/bold]:question_mark:")
        question_6_rel(conn)
    with tmp_connection("password", "questions/json/json.sql") as conn:
        CONSOLE.print(f"[bold]{'_'*30}JSON{'_'*30}[/bold]")
        CONSOLE.print("\n[bold]QUESTION 1[/bold]:question_mark:")
        question_1_json(conn)
        CONSOLE.print("\n[bold]QUESTION 2[/bold]:question_mark:")
        question_2_json(conn)
        CONSOLE.print("\n[bold]QUESTION 3[/bold]:question_mark:")
        question_3_json(conn)
        CONSOLE.print("\n[bold]QUESTION 4[/bold]:question_mark:")
        question_4_json(conn)
        CONSOLE.print("\n[bold]QUESTION 5[/bold]:question_mark:")
        question_5_json(conn)
        CONSOLE.print("\n[bold]QUESTION 6[/bold]:question_mark:")
        question_6_json(conn)


if __name__ == "__main__":
    main()
