# Demo-py-linq-sql

Pour lancer la demo vous devez lancer un container docker avec toutes les
dépendances préalablement installées.

Pour cela suivez les différente étapes.

## Prérequis

- Avoir postgresql installé sur la machine: https://www.postgresql.org/download/

## Installation

1. Clonez le projet
```sh
$ git clone https://gitlab.obspm.fr/exoplanet/demo-py-linq-sql.git
...
$ cd demo_py_linq_sql
...
```

2. Installez les dépendances
Soit avec poetry
```
$ poetry install
$ poetry shell
```
Soit qvec pip
```
pip install -r requirement.txt
```

3. Maintenant vous pouvez lancer le main avec la commande
```sh
python main.py
```

**Les question se trouve dans le dossier questions à la racine du projet dasn les fichiers:**

- `questions/relational/questions_relqtional.py`
- `questions/json/questions_json.py`
