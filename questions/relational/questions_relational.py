from py_linq_sql import SQLEnumerable, concat
from psycopg import Connection
from ..utils import pretty_print

# Pour toutes les questions vous trouverez
# la documentation ici: https://py-linq-sql.readthedocs.io/en/latest/
#
# Toute les questions sont faisable en une seul requete py-linq-sql.
#
# Vous pouvez voir le resultat de vos requetes en lancant:
# `python main.py` á la racine du projet.


def question_1(conn: Connection) -> None:
    """
    Consigne:
        A l'aide d'une requete py-linq-SQL,
        afficher toute les ligne de la table `identity`.

    Tips:
        SQLEnumerable.select
        https://py-linq-sql.readthedocs.io/en/latest/doc/examples/examples/#select
    """
    result = SQLEnumerable(conn, "<table>")
    pretty_print(result)


def question_2(conn: Connection) -> None:
    """
    Consigne:
        A l'aide d'une requete py-linq-SQL,
        afficher tout les `first_name` de la table `name`.

    Tips:
        SQLEnumerable.select
        https://py-linq-sql.readthedocs.io/en/latest/doc/examples/examples/#select
    """
    result = SQLEnumerable(conn, "<table>")
    pretty_print(result)


def question_3(conn: Connection) -> None:
    """
    Consigne:
        A l'aide d'une requete py-linq-SQL,
        afficher les `nickname` de la table `identity` où
        le `family_name` est 'Riddle'.

    Tips:
        SQLEnumerable.where
        https://py-linq-sql.readthedocs.io/en/latest/doc/examples/examples/#where
    """
    result = SQLEnumerable(conn, "<table>")
    pretty_print(result)


def question_4(conn: Connection) -> None:
    """
    Consigne:
        A l'aide d'une requete py-linq-SQL,
        inserer une nouvelle ligne dans la table `name`
        avec comme donnée: first_name = "Marvolo", identity = 1.

    Tips:
        SQLEnumerable.simple_insert
        https://py-linq-sql.readthedocs.io/en/latest/doc/examples/examples/#simple-insert
    """
    SQLEnumerable(conn, "<table>")

    # Un insert renvoie le nombre de ligne insérées,
    # à la place nous affichons la table `name`.
    pretty_print(SQLEnumerable(conn, "name").select().execute())


def question_5(conn: Connection) -> None:
    """
    Consigne:
        A l'aide d'une *seule* requete py-linq-SQL,
        afficher tout les `first_name`, `family_name` et `nickname`,
        liés au nom de famille est 'Riddle',
        des tables `name` et `identity`.

    Tips:
        SQLEnumerable.join
        https://py-linq-sql.readthedocs.io/en/latest/doc/examples/examples/#join
    """
    result = (
        SQLEnumerable(conn, "identity")
        .join()
        .where(lambda x: x.identity.family_name == "Riddle")
        .execute()
    )

    pretty_print(result)


def question_6(conn: Connection) -> None:
    """
    Consigne:
        A l'aide d'une *seule* requete py-linq-SQL,
        afficher tout les `family_name`, `nickname` et la concatenation des
        `name` des tables `name` et `identity`.

    Tips:
        SQLEnumerable.group_join
        https://py-linq-sql.readthedocs.io/en/latest/doc/examples/examples/#group-join
    """
    result = SQLEnumerable(conn, "<table>")
    pretty_print(result)
