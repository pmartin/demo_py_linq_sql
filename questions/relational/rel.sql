-- Create table and insert data for the demo

-- Create table

CREATE TABLE identity (
    id serial NOT NULL PRIMARY KEY,
    family_name text NOT NULL,
    nickname text
);

CREATE TABLE name (
    id serial NOT NULL PRIMARY KEY,
    identity integer NOT NULL references identity(id),
    first_name text NOT NULL
);


-- INSERT DATA

-- Voldemort
INSERT INTO identity(family_name, nickname) VALUES('Riddle', 'Lord Voldemort'); -- 1
INSERT INTO name(identity, first_name) VALUES (1, 'Tom'); -- 1
