from py_linq_sql import SQLEnumerable, pretty_print as pylinqsql_pretty_print
from py_linq import Enumerable
from rich.console import Console

CONSOLE = Console()


def pretty_print(data: object) -> None:
    if isinstance(data, SQLEnumerable):
        CONSOLE.print("L'exercice n'est pas résolue. :cross_mark:\n")
    elif isinstance(data, Enumerable):
        pylinqsql_pretty_print(data)
        print("\n")
    else:
        CONSOLE.print(data)
        print("\n")


def print_consigne() -> None:
    CONSOLE.print(
        "\n----------------------------------------------------------------------"
    )
    CONSOLE.print(
        "| Toutes les questions se trouve dans le dossier: questions/ .       |"
    )
    CONSOLE.print(
        "| questions/relational/questions_relational.py .                     |"
    )
    CONSOLE.print(
        "| questions/json/questions_json.py .                                 |\n"
        "|                                                                    |"
    )
    CONSOLE.print(
        "| Pour toutes les questions vous trouverez la documentation ici:     |\n"
        "|\t[link]https://py-linq-sql.readthedocs.io/en/latest/[/link]"
        "                |\n"
        "|                                                                    |",
    )
    CONSOLE.print(
        "| [bold]Toute les questions sont faisable en une seul requete py-linq-sql."
        "[/bold] "
        "|\n|                                                                    |",
    )
    CONSOLE.print(
        "| Vous pouvez voir le resultat de vos requetes en lancant:           |\n|"
        "\t`python main.py`                                             |\n| à la "
        "racine du projet.                                             |"
    )
    CONSOLE.print(
        "----------------------------------------------------------------------"
    )
