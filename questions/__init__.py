from .utils import print_consigne
from .relational import (
    question_1_rel,
    question_2_rel,
    question_3_rel,
    question_4_rel,
    question_5_rel,
    question_6_rel,
)

from .json import (
    question_1_json,
    question_2_json,
    question_3_json,
    question_4_json,
    question_5_json,
    question_6_json,
)
