-- Create table and insert data for the demo

-- Create table

CREATE TABLE identity (
    id serial NOT NULL PRIMARY KEY,
    data jsonb NOT NULL
);
CREATE INDEX dataidentity ON identity USING gin(data);

CREATE TABLE creature (
    id serial NOT NULL PRIMARY KEY,
    data jsonb NOT NULL
);
CREATE INDEX datacreature ON creature USING gin(data);

-- INSERT DATA

-- Voldemort
INSERT INTO identity(data) VALUES
('{"first_name": ["Tom"], "family_name": "Riddle", "nickname": "Lord Voldemort"}');

-- Nagini
INSERT INTO creature(data) VALUES
('{"name": "Nagini", "species": "snake", "owner": "Lord Voldemort"}');

-- Basilik in the Chamber of Secrets
INSERT INTO creature(data) VALUES
('{"name": "Basilik of the Chamber of Secrets", "species": "snake", "owner": "Lord Voldemort"}');
