from py_linq_sql import SQLEnumerable, concat
from psycopg import Connection
from ..utils import pretty_print

# Pour toutes les questions vous trouverez
# la documentation ici: https://py-linq-sql.readthedocs.io/en/latest/
#
# Toute les questions sont faisable en une seul requete py-linq-sql.
#
# Vous pouvez voir le resultat de vos requetes en lancant:
# `python main.py` á la racine du projet.


def question_1(conn: Connection) -> None:
    """
    Consigne:
        A l'aide d'une requete py-linq-SQL,
        afficher toute les ligne de la table `identity`.

    Tips:
        SQLEnumerable.select
    """
    result = SQLEnumerable(conn, "identity").select().execute()
    pretty_print(result)


def question_2(conn: Connection) -> None:
    """
    Consigne:
        A l'aide d'une requete py-linq-SQL,
        afficher tout les `first_name` de la table `identity`.

    Tips:
        SQLEnumerable.select

    """
    result = (
        SQLEnumerable(conn, "identity").select(lambda x: x.data.first_name).execute()
    )
    pretty_print(result)


def question_3(conn: Connection) -> None:
    """
    Consigne:
        A l'aide d'une requete py-linq-SQL,
        afficher les `nickname` de la table `identity` où
        le `family_name` est 'Riddle'.

    Tips:
        SQLEnumerable.where
    """
    result = (
        SQLEnumerable(conn, "identity")
        .select(lambda x: x.data.nickname)
        .where(lambda x: x.data.family_name == "Riddle")
        .execute()
    )
    pretty_print(result)


def question_4(conn: Connection) -> None:
    """
    Consigne:
        A l'aide d'une requete py-linq-SQL,
        inserer une nouvelle ligne dans la table `identity`
        avec comme doneée: first_name = ["Delphi"], family_name = "Riddle.

    Tips:
        SQLEnumerable.insert
    """
    SQLEnumerable(conn, "identity").insert(
        "data",
        {
            "first_name": ["Delphi"],
            "family_name": "Riddle",
        },
    ).execute()

    # Un insert renvoie le nombre de ligne insérées,
    # à la place nous affichons la table `identity`.
    pretty_print(SQLEnumerable(conn, "identity").select().execute())


def question_5(conn: Connection) -> None:
    """
    Consigne:
        A l'aide d'une *seul* requete py-linq-SQL,
        afficher tout les `name` et `species` des creatures
        ainsi que les `family_name` et `nickname` des proprietaires de creature.

    Tips:
        SQLEnumerable.join
    """
    result = (
        SQLEnumerable(conn, "identity")
        .join(
            inner=SQLEnumerable(conn, "creature"),
            outer_key=lambda identity: identity.data.nickname,
            inner_key=lambda creature: creature.data.owner,
            result_function=lambda creature, identity: {
                "creature_name": creature.data.name,
                "creature_species": creature.data.species,
                "owner_nickname": identity.data.nickname,
                "owner_family_name": identity.data.family_name,
            },
        )
        .execute()
    )
    pretty_print(result)


def question_6(conn: Connection) -> None:
    """
    Consigne:
        A l'aide d'une *seul* requete py-linq-SQL,
        afficher la concatenation des `name` de toutes les creatures appartenant à un
        proprietaire ainsi que le `nickname` de celui ci

    Tips:
        SQLEnumerable.group_join
    """
    result = (
        SQLEnumerable(conn, "identity")
        .group_join(
            inner=SQLEnumerable(conn, "creature"),
            outer_key=lambda identity: identity.data.nickname,
            inner_key=lambda creature: creature.data.owner,
            result_function=lambda creature, identity: {
                "creature_name": concat(creature.data.name, separator=" , "),
                "owner_nickname": identity.data.nickname,
            },
        )
        .execute()
    )
    pretty_print(result)
