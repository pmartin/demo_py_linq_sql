from mpmath import cos as mpm_cos
from mpmath import libmp, mp, mpf
from mpmath import nstr as mpf_to_str
from mpmath import radians as mpm_radians
from mpmath import sin as mpm_sin
from typing import NamedTuple
from decimal import Decimal

mp.dps = 20


class CharacterPosition(NamedTuple):
    ra: Decimal
    dec: Decimal


def _degree_to_radian_decimal(number: Decimal) -> Decimal:
    """
    Get an angle in radians and decimal of a degrees value.

    We choose 20 significant digits (precision). If the number is less than 1 and
    greater than -1 it gives 20 deicmal digits.
    Otherwise it gives X digits before the decimal and 20 - X decimal digits.

    For example:
    A = 1.0723692508781812416
    B = 0.12452756813273719415

    A and B both have 20 significant digits.

    Args:
        number: A number in degrees which we want the radians.

    Returns:
        Cosinu in rafians and decimal of a degrees value.

    Examples:
        >>> _degree_to_radian_decimal(Decimal(12))
        Decimal('0.20943951023931954923')
    """
    return Decimal(
        mpf_to_str(
            mpm_radians(mpf(libmp.from_Decimal(number))),
            20,
            strip_zeros=False,
        ),
    )


def sind_decimal(number: Decimal) -> Decimal:
    """
    Get the sin in decimal of a degrees value.

    We choose 20 significant digits (precision). If the number is less than 1 and
    greater than -1 it gives 20 deicmal digits.
    Otherwise it gives X digits before the decimal and 20 - X decimal digits.

    For example:
    A = 1.0723692508781812416
    B = 0.12452756813273719415

    A and B both have 20 significant digits.

    Args:
        number: A number in degrees which we want the sinus.

    Returns:
        Sinus in radians and decimal of a degrees value.

    Examples:
        >>> _sind_decimal(Decimal(12))
        Decimal('0.20791169081775933710')
    """
    number_in_radians = _degree_to_radian_decimal(number)

    return Decimal(
        mpf_to_str(
            mpm_sin(mpf(libmp.from_Decimal(number_in_radians))),
            20,
            strip_zeros=False,
        ),
    )


def cosd_decimal(number: Decimal) -> Decimal:
    """
    Get the cos in decimal of a degrees value.

    We choose 20 significant digits (precision). If the number is less than 1 and
    greater than -1 it gives 20 deicmal digits.
    Otherwise it gives X digits before the decimal and 20 - X decimal digits.

    For example:
    A = 1.0723692508781812416
    B = 0.12452756813273719415

    A and B both have 20 significant digits.

    Args:
        number: A number in degrees which we want the cosine.

    Returns:
        Cosinu in rafians and decimal of a degrees value.

    Examples:
        >>> _cosd_decimal(Decimal(12))
        Decimal('0.97814760073380563793')
    """
    number_in_radians = _degree_to_radian_decimal(number)

    return Decimal(
        mpf_to_str(
            mpm_cos(mpf(libmp.from_Decimal(number_in_radians))),
            20,
            strip_zeros=False,
        ),
    )
