-- Create table and insert data for the demo

-- Create table

CREATE TABLE identity (
    id serial NOT NULL PRIMARY KEY,
    data jsonb NOT NULL
);
CREATE INDEX dataidentity ON identity USING gin(data);

CREATE TABLE creature (
    id serial NOT NULL PRIMARY KEY,
    data jsonb NOT NULL
);
CREATE INDEX datacreature ON creature USING gin(data);

CREATE TABLE position (
    id serial NOT NULL PRIMARY KEY,
    ra decimal NOT NULL,
    dec decimal NOT NULL,
    identity_id int references identity(id)
);

-- INSERT DATA

-- Voldemort
INSERT INTO identity(data) VALUES
('{"first_name": ["Tom, Marvolo"], "family_name": "Riddle", "nickname": "Lord Voldemort"}');

INSERT INTO identity(data) VALUES
('{"first_name": ["Harry", "James"], "family_name": "Potter"}');

-- Nagini
INSERT INTO creature(data) VALUES
('{"name": "Nagini", "species": "snake", "owner": "Lord Voldemort"}');

-- Basilik in the Chamber of Secrets
INSERT INTO creature(data) VALUES
('{"name": "Basilik of the Chamber of Secrets", "species": "snake", "owner": "Lord Voldemort"}');

-- Position of Voldemort
INSERT INTO position(ra, dec, identity_id) VALUES (220, 40, 1);

-- Position of Harry Potter
INSERT INTO position(ra, dec, identity_id) VALUES (130, -40, 2);
