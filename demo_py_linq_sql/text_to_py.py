from tmp_connection_psql import tmp_connection
from py_linq_sql import SQLEnumerable, pretty_print
from rich.console import Console

CONSOLE = Console()

with tmp_connection("password", "demo_py_linq_sql/demo.sql") as conn:

    # Version sans py_linq_sql

    CONSOLE.print("\n[bold]___SANS PY-LINQ-SQL___[/bold]")

    query = (
        """SELECT "identity"."data"->'family_name' AS family_name, """
        """"identity"."data"->'first_name' AS first_name, """
        """"position"."ra" AS position_ra, "position"."dec" AS position_dec """
        """FROM "identity" INNER JOIN "position" """
        """ON ("identity"."id")::text = ("position"."identity_id")::text """
        """LIMIT 1 OFFSET 1"""
    )

    cursor = conn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()

    CONSOLE.print(f"[bold italic]requete:[/bold italic] {query}")
    CONSOLE.print(
        f"\n[bold italic]resultat:[/bold italic] resultat de la requete: {result}"
    )

    CONSOLE.print("\n[bold]___AVEC PY-LINQ-SQL___[/bold]")

    # Avec py_linq_sql

    sqle = (
        SQLEnumerable(conn, "identity")
        .join(
            inner=SQLEnumerable(conn, "position"),
            outer_key=lambda identity: identity.id,
            inner_key=lambda position: position.identity_id,
            result_function=lambda position, identity: {
                "family_name": identity.data.family_name,
                "first_name": identity.data.first_name,
                "position_ra": position.ra,
                "position_dec": position.dec,
            },
        )
        .skip(1)
        .take(1)
    )

    CONSOLE.print(
        "[bold italic]requete générée par py-lnq-sql:"
        f"[/bold italic] {sqle.get_command()}"
    )
    CONSOLE.print("[bold italic]resultat de la requete:[/bold italic]")
    pretty_print(sqle.execute())
