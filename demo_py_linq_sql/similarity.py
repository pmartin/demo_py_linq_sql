"""Functions for the similarity of objects."""

# Standard imports
from decimal import Decimal

# Third party imports
from psycopg import Connection
from py_linq_sql import SQLEnumerable
from py_linq_sql import acosd as pylinqsql_acosd
from py_linq_sql import cosd as pylinqsql_cosd
from py_linq_sql import sind as pylinqsql_sind
from .utils import CharacterPosition, sind_decimal, cosd_decimal


def is_at_the_same_position(
    conn: Connection,
    table: str,
    character_to_check: CharacterPosition,
    epsilon: Decimal,
) -> bool:
    exactly_same_ra_dec = (
        SQLEnumerable(conn, table)
        .select(lambda x: x.id)
        .where(
            lambda x: (x.dec == character_to_check.dec)
            & (x.ra == character_to_check.ra)
        )
        .execute()
    )

    if exactly_same_ra_dec.to_list():
        return True

    epsilon_in_degrees = epsilon / Decimal("3600.0")

    sam_position_near_epsilon = (
        SQLEnumerable(conn, table)
        .select(lambda x: x.id)
        .where(
            lambda x: (x.dec >= character_to_check.dec - epsilon_in_degrees)
            & (x.dec <= character_to_check.dec + epsilon_in_degrees),
        )
        .where(
            lambda x: (
                pylinqsql_acosd(
                    pylinqsql_sind(x.dec) * sind_decimal(character_to_check.dec)
                    + pylinqsql_cosd(x.dec)
                    * cosd_decimal(character_to_check.dec)
                    * pylinqsql_cosd(x.ra - character_to_check.ra),
                )
                < epsilon_in_degrees
            ),
        )
        .execute()
    )

    if sam_position_near_epsilon.to_list():
        return True

    return False
