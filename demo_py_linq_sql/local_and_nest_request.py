from tmp_connection_psql import tmp_connection
from py_linq_sql import SQLEnumerable
from rich.console import Console

CONSOLE = Console()

with tmp_connection("password", "demo_py_linq_sql/demo_max.sql") as conn:
    # local request on an executed SQL request

    CONSOLE.print("\n[bold]___Requete en local___[/bold]")

    sql_req = (
        SQLEnumerable(conn, "point")
        .select(lambda x: x.data.distance)
        .take(200)
        .execute()
    )
    local_req = sql_req.max(lambda x: x.data_distance)

    CONSOLE.print(
        "[bold italic]resultat de la commande puis de la requete "
        f"en local:[/bold italic] {local_req}"
    )

    # Nested request

    CONSOLE.print("\n[bold]___Requete imbirquée sur le serveur___[/bold]")

    sql_take = SQLEnumerable(conn, "point").select().take(499)
    sql_max = (
        SQLEnumerable(conn, sql_take)
        .select()
        .max(lambda x: x.data.distance, cast_type=int)
    )

    CONSOLE.print(
        "[bold italic]command générée par py-linq-sql:"
        f"[/bold italic] {sql_max.get_command()}"
    )
    CONSOLE.print(
        f"[bold italic]résultat de la commande:[/bold italic] {sql_max.execute()}"
    )
